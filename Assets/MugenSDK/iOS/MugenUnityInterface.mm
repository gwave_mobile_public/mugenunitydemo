//
//  MugenUnityInterface.m
//  Mugen
//
//  Created by lijunsheng on 2024/1/24.
//

// MugenUnityInterface.mm

#import <Foundation/Foundation.h>
#import "MugenUnity.h"
#import <objc/runtime.h>  // 确保导入此头文件




extern "C" {
    // 导出到Unity的C接口

    void MugenUnity_CallDeviceLogin() {
        [MugenUnity deviceLogin];
    }
    
    void MugenUnity_CallInitSDK(const char* envType, const char* network, const char* clientId, const char* web3Network, const char* url) {
        NSString *envTypeStr = [NSString stringWithUTF8String:envType];
        NSString *networkStr = [NSString stringWithUTF8String:network];
        NSString *clientIdStr = [NSString stringWithUTF8String:clientId];
        NSString *web3NetworkStr = [NSString stringWithUTF8String:web3Network];
        NSString *urlStr = [NSString stringWithUTF8String:url];
        [MugenUnity initSDK:envTypeStr network:networkStr clientId:clientIdStr web3Network:web3NetworkStr url:urlStr];
    }

    void MugenUnity_CallGetLocalUserInfo() {
        [MugenUnity getLocalUserInfo];
    }

    void MugenUnity_CallGetUserInfo() {
        [MugenUnity getUserInfo];
    }

    void MugenUnity_CallWeb3AuthSignIn(const char* provider, const char* email, bool autoRegister, bool web3Bind, const char* walletType) {
        NSString *providerStr = [NSString stringWithUTF8String:provider];
        NSString *emailStr = @"";
        if (email != NULL) {
            emailStr = [NSString stringWithUTF8String:email];
        }
        NSString *walletTypeStr = [NSString stringWithUTF8String:walletType];
        [MugenUnity Web3AuthSignIn:providerStr email:emailStr autoRegister:autoRegister web3Bind:web3Bind walletType:walletTypeStr];
    }

    void MugenUnity_CallGetEmailCode(const char* email) {
        NSString *emailStr = [NSString stringWithUTF8String:email];
        [MugenUnity getEmailCode:emailStr];
    }

    void MugenUnity_CallQuickLogin(const char* email, const char* emailVerifyCode) {
        NSString *emailStr = [NSString stringWithUTF8String:email];
        NSString *emailVerifyCodeStr = [NSString stringWithUTF8String:emailVerifyCode];
        [MugenUnity quickLogin:emailStr codeStr:emailVerifyCodeStr];
    }

    void MugenUnity_CallOAuthBind(const char* provider, const char* email, const char* walletType) {
        NSString *providerStr = [NSString stringWithUTF8String:provider];
        NSString *emailStr = @"";
        if (email != NULL) {
            emailStr = [NSString stringWithUTF8String:email];
        }
        NSString *walletTypeStr = [NSString stringWithUTF8String:walletType];
        [MugenUnity oAuthBind:providerStr email:emailStr walletType:walletTypeStr];
    }

    void MugenUnity_CallLogout() {
        [MugenUnity logout];
    }

    void MugenUnity_CallGetJwtToken() {
        [MugenUnity getJwtToken];
    }

    void MugenUnity_CallEmailBind(const char* email, const char* emailVerifyCode, const int type) {
        NSString *emailStr = [NSString stringWithUTF8String:email];
        NSString *emailVerifyCodeStr = [NSString stringWithUTF8String:emailVerifyCode];
        [MugenUnity emailBind:emailStr codeStr:emailVerifyCodeStr type:type];
    }
}
