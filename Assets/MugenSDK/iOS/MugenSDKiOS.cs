using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;


namespace com.Mugen.sdk
{
    public class MugenSDKiOS
    {

        public event EventHandler<string> OnLoginResponseReceived;

        public static MugenSDKiOS _mugenSDKiOS = new MugenSDKiOS();

        public static MugenSDKiOS GetInstance()
        {
            return _mugenSDKiOS;
        }

        // 导入 Objective-C 中定义的函数
        [DllImport("__Internal")]
        private static extern void MugenUnity_CallInitSDK(string environmentType, string network, string clientId, string web3Network, string url);

        [DllImport("__Internal")]
        private static extern void MugenUnity_CallDeviceLogin();


        [DllImport("__Internal")]
        private static extern void MugenUnity_CallGetLocalUserInfo();

        [DllImport("__Internal")]
        private static extern void MugenUnity_CallGetUserInfo();

        [DllImport("__Internal")]
        private static extern void MugenUnity_CallWeb3AuthSignIn(string provider, string email, Boolean autoRegister, Boolean web3Bind,string walletType);

        [DllImport("__Internal")]
        private static extern void MugenUnity_CallGetEmailCode(string email);

        [DllImport("__Internal")]
        private static extern void MugenUnity_CallQuickLogin(string email, string emailVerifyCode);

        [DllImport("__Internal")]
        private static extern void MugenUnity_CallOAuthBind(string provider, string email,string walletType);

        [DllImport("__Internal")]
        private static extern void MugenUnity_CallLogout();

        [DllImport("__Internal")]
        private static extern void MugenUnity_CallGetJwtToken();

        [DllImport("__Internal")]
        private static extern void MugenUnity_CallEmailBind(string email, string emailVerifyCode,int type);

        // 调用初始化 SDK 的方法
        public static void CallInitSDK(string environmentType, string network, string clientId, string web3Network, string url)
        {
            MugenUnity_CallInitSDK(environmentType, network, clientId, web3Network, url);
        }

        // 调用设备登录方法
        public static void CallDeviceLogin()
        {
            MugenUnity_CallDeviceLogin();
        }

        public static void CallWeb3AuthSignIn(string provider, string email, Boolean autoRegister, Boolean web3Bind,string walletType)
        {
            MugenUnity_CallWeb3AuthSignIn(provider, email, autoRegister, web3Bind, walletType);
        }

        public static void CallGetUserInfo()
        {
            MugenUnity_CallGetUserInfo();
        }

        public static void CallGetLocalUserInfo()
        {
            MugenUnity_CallGetLocalUserInfo();
        }

        public static void CallGetEmailCode(string email)
        {
            MugenUnity_CallGetEmailCode(email);
        }

        public static void CallQuickLogin(string email, string emailVerifyCode)
        {
            MugenUnity_CallQuickLogin(email, emailVerifyCode);
        }

        public static void CallOAuthBind(string provider, string email,string walletType)
        {
            MugenUnity_CallOAuthBind(provider, email, walletType);
        }

        public static void CallLogout()
        {
            MugenUnity_CallLogout();
        }

        public static void CallGetJwtToken()
        {
            MugenUnity_CallGetJwtToken();
        }

        public static void CallEmailBind(string email, string emailVerifyCode, int type)
        {
            MugenUnity_CallEmailBind(email, emailVerifyCode, type);
        }

    }
}