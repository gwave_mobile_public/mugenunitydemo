//
//  MugenUnity.h
//  Mugen
//
//  Created by lijunsheng on 2024/1/24.
//

#ifndef MugenUnity_h
#define MugenUnity_h

#ifndef dispatch_main_async_safe
#define dispatch_main_async_safe(block)\
    if (dispatch_queue_get_label(DISPATCH_CURRENT_QUEUE_LABEL) == dispatch_queue_get_label(dispatch_get_main_queue())) {\
        block();\
    } else {\
        dispatch_async(dispatch_get_main_queue(), block);\
    }
#endif

typedef void (^DeviceLoginCallBack)(id value);
typedef void (^Web3AuthLoginCallBack)(id value);
typedef void (^VerifyCodeCallBack)(id value);

// MugenUnity.h

#import <Foundation/Foundation.h>

@interface MugenUnity : NSObject

+ (void)initSDK:(NSString *)envType network:(NSString *)netWork clientId:(NSString *)clientId web3Network:(NSString *)web3Network url:(NSString *)url;
+ (void)deviceLogin;
+ (void)getLocalUserInfo;
+ (void)getUserInfo;
+ (void)Web3AuthSignIn:(NSString *)provider email:(NSString *)email autoRegister:(BOOL)autoRegister web3Bind:(BOOL)web3Bind walletType:(NSString *)walletType;
+ (void)quickLogin:(NSString *)emailStr codeStr:(NSString *)codeStr;
+ (void)getEmailCode:(NSString *)emailStr;
+ (void)getJwtToken;
+ (void)oAuthBind:(NSString *)provider email:(NSString *)email walletType:(NSString *)walletType;
+ (void)emailBind:(NSString *)emailStr codeStr:(NSString *)codeStr type:(int)type;
+ (void)logout;

@end

@interface DeviceLoginDelegate : NSObject

@property (nonatomic, copy) DeviceLoginCallBack deviceLoginCallBack;

@end

@interface Web3AuthLoginDelegate : NSObject

@property (nonatomic, copy) Web3AuthLoginCallBack web3AuthLoginCallBack;

@end

@interface VerifyCodeDelegate : NSObject

@property (nonatomic, copy) VerifyCodeCallBack verifyCodeCallBack;

@end

#endif /* MugenUnity_h */
