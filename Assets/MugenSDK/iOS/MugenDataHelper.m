//
//  MugenDataHelper.m
//  UnityFramework
//
//  Created by 杨鹏 on 2024/2/6.
//

#import "MugenDataHelper.h"
#import <MugenContainer/MugenContainer.h>

@implementation MugenDataHelper

+ (NSDictionary *)convertToUserInfoJsDict:(id)dataDict
{
    NSMutableDictionary *containDict = [self formatUserInfo:dataDict];
    return containDict.copy;
}

+ (NSDictionary *)convertToLoginInfoJsDict:(id)userInfo web3AuthInfo:(id)web3AuthInfo
{
    NSMutableDictionary *containDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *userDict = [self formatUserInfo:userInfo];
    NSMutableDictionary *web3AuthDict = [self formatWeb3UserInfo:web3AuthInfo];
    [containDict setObject:userDict forKey:@"loginUserInfo"];
    [containDict setObject:web3AuthDict forKey:@"web3UserInfo"];
    return containDict.copy;
}

+ (NSDictionary *)convertToCustomerJsDict:(MugenContainerWebResultCustomerVO *)data
{
    NSMutableDictionary *containDict = [[NSMutableDictionary alloc] init];
    [containDict setObject:data.success?:@"" forKey:@"success"];
    [containDict setObject:data.msgKey?:@"" forKey:@"msgKey"];
    [containDict setObject:data.code?:@"" forKey:@"code"];
    [containDict setObject:[self formatMugenCustomerVO:data] forKey:@"obj"];
    return containDict.copy;
}

+ (NSDictionary *)convertToWebResult:(MugenContainerWebResult *)data
{
    NSMutableDictionary *containDict = [[NSMutableDictionary alloc] init];
    [containDict setObject:data.success?:@"" forKey:@"success"];
    [containDict setObject:data.msgKey?:@"" forKey:@"msgKey"];
    [containDict setObject:data.code?:@"" forKey:@"code"];
    [containDict setObject:data.responseEnum?:@"" forKey:@"responseEnum"];
    return containDict.copy;
}

+ (NSMutableDictionary *)formatWeb3UserInfo:(MugenContainerAuthResponse *)data
{
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    [dataDict setObject:data.address?:@"" forKey:@"address"];
    [dataDict setObject:data.idToken?:@"" forKey:@"idToken"];
    [dataDict setObject:data.publicKey?:@"" forKey:@"publicKey"];
    [dataDict setObject:data.privateKey?:@"" forKey:@"privateKey"];

    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
    [userInfo setObject:data.userInfo.aggregateVerifier?:@"" forKey:@"aggregateVerifier"];
    [userInfo setObject:data.userInfo.dappShare?:@"" forKey:@"dappShare"];
    [userInfo setObject:data.userInfo.email?:@"" forKey:@"email"];
    [userInfo setObject:data.userInfo.idToken?:@"" forKey:@"idToken"];
    [userInfo setObject:data.userInfo.name?:@"" forKey:@"name"];
    [userInfo setObject:data.userInfo.oAuthIdToken?:@"" forKey:@"oAuthIdToken"];
    [userInfo setObject:data.userInfo.oAuthAccessToken?:@"" forKey:@"oAuthAccessToken"];
    [userInfo setObject:data.userInfo.profileImage?:@"" forKey:@"profileImage"];
    [userInfo setObject:data.userInfo.typeOfLogin?:@"" forKey:@"typeOfLogin"];
    [userInfo setObject:data.userInfo.verifier?:@"" forKey:@"verifier"];
    [userInfo setObject:data.userInfo.verifierId?:@"" forKey:@"verifierId"];
    [dataDict setObject:userInfo forKey:@"userInfo"];

    return dataDict;
}

+ (NSMutableDictionary *)formatUserInfo:(MugenContainerUserInfo *)data
{
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    [dataDict setObject:[self formatMugenCustomerVO:data.customerInfo] forKey:@"customerInfo"];
    [dataDict setObject:[self formatMugenCustomerInviteVO:data.inviteInfo] forKey:@"inviteInfo"];
    return dataDict;
}

+ (NSDictionary *)formatMugenCustomerVO:(MugenContainerWebResultCustomerVO *)customerVO
{
    NSMutableDictionary *customerInfo = [[NSMutableDictionary alloc] init];
    if (customerVO.success.boolValue) {
    [customerInfo setObject:customerVO.obj.isPhoneCertified?:@"" forKey:@"isPhoneCertified"];
    [customerInfo setObject:customerVO.obj.isGoogleCertified?:@"" forKey:@"isGoogleCertified"];
    [customerInfo setObject:customerVO.obj.gender?:@"" forKey:@"gender"];
    [customerInfo setObject:customerVO.obj.isBindEmail?:@"" forKey:@"isBindEmail"];
    [customerInfo setObject:customerVO.obj.registerType?:@"" forKey:@"registerType"];
    [customerInfo setObject:customerVO.obj.publicKey?:@"" forKey:@"publicKey"];
    [customerInfo setObject:customerVO.obj.locale?:@"" forKey:@"locale"];
    [customerInfo setObject:customerVO.obj.merchantName?:@"" forKey:@"merchantName"];
    [customerInfo setObject:customerVO.obj.isExistAssetPassword?:@"" forKey:@"isExistAssetPassword"];
    [customerInfo setObject:customerVO.obj.vipLevel?:@"" forKey:@"vipLevel"];
    [customerInfo setObject:customerVO.obj.isBindPhone?:@"" forKey:@"isBindPhone"];
    [customerInfo setObject:customerVO.obj.merchantId?:@"" forKey:@"merchantId"];
    [customerInfo setObject:customerVO.obj.idCertifiedStatus?:@"" forKey:@"idCertifiedStatus"];
    [customerInfo setObject:customerVO.obj.id?:@"" forKey:@"id"];
    [customerInfo setObject:customerVO.obj.email?:@"" forKey:@"email"];
    [customerInfo setObject:customerVO.obj.wsKey?:@"" forKey:@"wsKey"];
    [customerInfo setObject:customerVO.obj.isBindGoogle?:@"" forKey:@"isBindGoogle"];
    [customerInfo setObject:customerVO.obj.noPassword?:@"" forKey:@"noPassword"];
    [customerInfo setObject:customerVO.obj.nickName?:@"" forKey:@"nickName"];
    [customerInfo setObject:customerVO.obj.lineId?:@"" forKey:@"lineId"];
    [customerInfo setObject:customerVO.obj.avatar?:@"" forKey:@"avatar"];
    [customerInfo setObject:customerVO.obj.userName?:@"" forKey:@"userName"];
    [customerInfo setObject:customerVO.obj.telegramId?:@"" forKey:@"telegramId"];
    [customerInfo setObject:customerVO.obj.vipEndTime?:@"" forKey:@"vipEndTime"];
    [customerInfo setObject:customerVO.obj.kycLevel?:@"" forKey:@"kycLevel"];
    [customerInfo setObject:customerVO.obj.uniqueName?:@"" forKey:@"uniqueName"];
    [customerInfo setObject:customerVO.obj.phone?:@"" forKey:@"phone"];
    [customerInfo setObject:customerVO.obj.whatsAppId?:@"" forKey:@"whatsAppId"];
    [customerInfo setObject:customerVO.obj.isMigration?:@"" forKey:@"isMigration"];
    [customerInfo setObject:customerVO.obj.middleName?:@"" forKey:@"middleName"];
    [customerInfo setObject:customerVO.obj.region?:@"" forKey:@"region"];
    [customerInfo setObject:customerVO.obj.jwtKey?:@"" forKey:@"jwtKey"];
    [customerInfo setObject:customerVO.obj.walletAddress?:@"" forKey:@"walletAddress"];
    [customerInfo setObject:customerVO.obj.isBindWeb3?:@"" forKey:@"isBindWeb3"];
    [customerInfo setObject:customerVO.obj.status?:@"" forKey:@"status"];
    [customerInfo setObject:customerVO.obj.refreshToken?:@"" forKey:@"refreshToken"];
    }
    return customerInfo.copy;
}

+ (NSDictionary *)formatMugenCustomerInviteVO:(MugenContainerWebResultCustomerInviteVO *)inviteVO
{
    NSMutableDictionary *inviteInfo = [[NSMutableDictionary alloc] init];
    if (inviteVO.success.boolValue) {
    [inviteInfo setObject:inviteVO.obj.inviterId?:@"" forKey:@"inviterId"];
    [inviteInfo setObject:inviteVO.obj.registerTime?:@"" forKey:@"registerTime"];
    [inviteInfo setObject:inviteVO.obj.inviteCode?:@"" forKey:@"inviteCode"];
    [inviteInfo setObject:inviteVO.obj.customerId?:@"" forKey:@"customerId"];
    [inviteInfo setObject:inviteVO.obj.revisable?:@"" forKey:@"revisable"];
    [inviteInfo setObject:inviteVO.obj.inviteTime?:@"" forKey:@"inviteTime"];
    }
    return inviteInfo.copy;
}

+ (NSString *)convertToString:(NSDictionary *)dataDict
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataDict options:NSJSONWritingPrettyPrinted error:&error];

    if (!jsonData) {
        return error.localizedDescription;
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        return jsonString;
    }
}

@end
