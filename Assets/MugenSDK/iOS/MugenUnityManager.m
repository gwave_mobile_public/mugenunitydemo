//
//  MugenUnityManager.m
//  UnityFramework
//
//  Created by 杨鹏 on 2024/2/5.
//

#import "MugenUnityManager.h"
#import <MugenContainer/MugenContainer.h>

@interface MugenUnityManager()

@property (nonatomic, strong) MugenContainerWeb3AuthClient *web3AuthClient;

@end

@implementation MugenUnityManager

+ (instancetype)sharedInstance {
    static MugenUnityManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (MugenContainerWeb3AuthClient *)web3AuthClient
{
    if(!_web3AuthClient) {
        _web3AuthClient = [[MugenContainerWeb3AuthClient alloc] init];
    }
    return _web3AuthClient;
}

@end
