//
//  MugenDataHelper.h
//  UnityFramework
//
//  Created by 杨鹏 on 2024/2/6.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MugenDataHelper : NSObject

+ (NSDictionary *)convertToUserInfoJsDict:(id)dataDict;

+ (NSDictionary *)convertToLoginInfoJsDict:(id)userInfo web3AuthInfo:(id)web3AuthInfo;

+ (NSDictionary *)convertToCustomerJsDict:(id)data;

+ (NSDictionary *)convertToWebResult:(id)data;

+ (NSString *)convertToString:(NSDictionary *)dataDict;

@end

NS_ASSUME_NONNULL_END
