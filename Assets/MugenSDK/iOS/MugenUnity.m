//
//  MugenUnity.m
//  Mugen
//
//  Created by lijunsheng on 2024/1/24.
//

#import <Foundation/Foundation.h>
#import <MugenContainer/MugenContainer.h>
#import "MugenUnity.h"
#import "MugenUnityManager.h"
#import "MugenDataHelper.h"

@implementation MugenUnity

static MugenContainerApplicationContainer* instance = nil;

+(MugenContainerApplicationContainer*) GetMugenContainerInstance{
    if (!instance)
    {
        instance = [MugenContainerApplicationContainer alloc];
    }
    return  instance;
}

+ (void)initSDK:(NSString *)envType network:(NSString *)netWork clientId:(NSString *)clientId web3Network:(NSString *)web3Network url:(NSString *)url
{
    MugenContainerApplicationContainer *applicationContainer = [[MugenContainerApplicationContainer alloc] initWithConfig:^(MugenContainerApplicationContainerConfiguration * _Nonnull configuration) {}];
    [MugenContainerApplicationInitializer.companion initializeType:[self getEnvironmentType:envType] container:applicationContainer network:MugenContainerNetWork.mugen];
    MugenContainerApplicationInitializer.companion.sassId = @"mugen";
    if (clientId.length > 0) {
        // 实现Web3授权初始化逻辑
        NSString *bundleIdentifier= [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleIdentifier"];
        [[MugenUnityManager sharedInstance].web3AuthClient doInitWeb3AuthClientId:clientId web3Network:[self getNetwork:web3Network] url:url uri:bundleIdentifier];
    }
}

+ (void)deviceLogin {
    // 实现设备登录逻辑
    MugenContainerLoginRepository *loginRepository = [[MugenContainerLoginRepository alloc] init];
    DeviceLoginDelegate *delegate = [[DeviceLoginDelegate alloc] init];
    delegate.deviceLoginCallBack = ^(id value) {
        MugenContainerUserInfo *userInfo = value;
        NSDictionary *userDict = [MugenDataHelper convertToUserInfoJsDict:userInfo];
        NSString *jsonStr = [MugenDataHelper convertToString:userDict];
        const char *gameObject = "MugenSDKMono";
        const char *methodName = "ReceiveMessageFromIOS";
        const char *message = [jsonStr UTF8String];
        UnitySendMessage(gameObject, methodName, message);
    };
    [loginRepository loginLoginInterface:(id<MugenContainerLoginResponseInterface>)delegate completionHandler:^(NSError * _Nullable error) {}];
}

+ (void)getLocalUserInfo
{
    MugenContainerLoginRepository *loginRepository = [[MugenContainerLoginRepository alloc] init];
    [loginRepository getLocalUserInfoWithCompletionHandler:^(MugenContainerWebResultCustomerVO * _Nullable customer, NSError * _Nullable error) {
        NSDictionary *userDict = [MugenDataHelper convertToCustomerJsDict:customer];
        NSString *jsonStr = [MugenDataHelper convertToString:userDict];
        const char *gameObject = "MugenSDKMono";
        const char *methodName = "ReceiveMessageFromIOS";
        const char *message = [jsonStr UTF8String];
        UnitySendMessage(gameObject, methodName, message);
    }];
}

+ (void)getUserInfo
{
    MugenContainerLoginRepository *loginRepository = [[MugenContainerLoginRepository alloc] init];
    [loginRepository getUserInfoWithCompletionHandler:^(MugenContainerUserInfo * _Nullable userInfo, NSError * _Nullable error) {
        NSDictionary *userDict = [MugenDataHelper convertToUserInfoJsDict:userInfo];
        NSString *jsonStr = [MugenDataHelper convertToString:userDict];
        const char *gameObject = "MugenSDKMono";
        const char *methodName = "ReceiveMessageFromIOS";
        const char *message = [jsonStr UTF8String];
        UnitySendMessage(gameObject, methodName, message);
    }];
}

+ (void)Web3AuthSignIn:(NSString *)provider email:(NSString *)email autoRegister:(BOOL)autoRegister web3Bind:(BOOL)web3Bind walletType:(NSString *)walletType
{
    Web3AuthLoginDelegate *delegate = [[Web3AuthLoginDelegate alloc] init];
    delegate.web3AuthLoginCallBack = ^(id value) {
        MugenContainerAuthResponse *authResponse = value;
        if (authResponse.idToken.length <= 0 && authResponse.publicKey.length <= 0) {
            const char *gameObject = "MugenSDKMono";
            const char *methodName = "ReceiveMessageFromIOS";
            const char *message = "idToken or publicKey is null, Because user cancel login or web3Auth api timeout";
            UnitySendMessage(gameObject, methodName, message);
        } else {
            MugenContainerLoginRepository *loginRepository = [[MugenContainerLoginRepository alloc] init];
            dispatch_main_async_safe(^{
                if (web3Bind) {
                    [loginRepository oauthBindIdToken:authResponse.idToken thirdAuthSource:MugenContainerThirdAuthType.externalSocial platform:@"web3auth" walletType:[self getWalletType:walletType] completionHandler:^(MugenContainerWebResult * _Nullable response, NSError * _Nullable error) {
                        NSDictionary *resultDict = [MugenDataHelper convertToWebResult:response];
                        NSString *jsonStr = [MugenDataHelper convertToString:resultDict];
                        const char *gameObject = "MugenSDKMono";
                        const char *methodName = "ReceiveMessageFromIOS";
                        const char *message = [jsonStr UTF8String];
                        UnitySendMessage(gameObject, methodName, message);
                    }];
                } else {
                    [loginRepository oauthLoginIdToken:authResponse.idToken publicKey:authResponse.publicKey autoRegister:autoRegister thirdAuthSource:MugenContainerThirdAuthType.externalSocial walletType:[self getWalletType:walletType] completionHandler:^(MugenContainerUserInfo * _Nullable userInfo, NSError * _Nullable error) {
                        NSDictionary *userDict = [MugenDataHelper convertToLoginInfoJsDict:userInfo web3AuthInfo:authResponse];
                        NSString *jsonStr = [MugenDataHelper convertToString:userDict];
                        const char *gameObject = "MugenSDKMono";
                        const char *methodName = "ReceiveMessageFromIOS";
                        const char *message = [jsonStr UTF8String];
                        UnitySendMessage(gameObject, methodName, message);
                    }];
                }
            });
        }
    };
    [[MugenUnityManager sharedInstance].web3AuthClient signInSignProvider:[self getSingInProvider:provider] email:email walletType:[self getWalletType:walletType] web3AuthInterface:(id<MugenContainerWeb3AuthInterface>)delegate];
}

+ (void)quickLogin:(NSString *)emailStr codeStr:(NSString *)codeStr
{
    if (emailStr.length > 0 && codeStr.length > 0) {
        MugenContainerLoginRepository *loginRepository = [[MugenContainerLoginRepository alloc] init];
        MugenContainerInt *tokenType = [[MugenContainerInt alloc] initWithInt:2];
        [loginRepository quickLoginEmail:emailStr mobile:nil password:nil smsVerifyCode:nil emailVerifyCode:codeStr tokenType:tokenType source:nil token:@"12" pTokenType:@"ios" deviceId:nil userAgent:nil smsType:nil inviteCode:nil autoRegister:YES completionHandler:^(MugenContainerUserInfo * _Nullable userInfo, NSError * _Nullable error) {
            NSDictionary *userDict = [MugenDataHelper convertToUserInfoJsDict:userInfo];
            NSString *jsonStr = [MugenDataHelper convertToString:userDict];
            const char *gameObject = "MugenSDKMono";
            const char *methodName = "ReceiveMessageFromIOS";
            const char *message = [jsonStr UTF8String];
            UnitySendMessage(gameObject, methodName, message);
        }];
    } else {
        const char *gameObject = "MugenSDKMono";
        const char *methodName = "ReceiveMessageFromIOS";
        const char *message = "email or verifyCode is null";
        UnitySendMessage(gameObject, methodName, message);
    }
}

+ (void)getEmailCode:(NSString *)emailStr
{
    if (emailStr.length > 0) {
        VerifyCodeDelegate *delegate = [[VerifyCodeDelegate alloc] init];
        delegate.verifyCodeCallBack = ^(id value) {
            MugenContainerLoginRepository *loginRepository = [[MugenContainerLoginRepository alloc] init];
            [loginRepository getEmailCodeEmail:emailStr completionHandler:^(MugenContainerWebResponse * _Nullable response, NSError * _Nullable error) {
                const char *gameObject = "MugenSDKMono";
                const char *methodName = "ReceiveMessageFromIOS";
                const char *message = "send email code succcess";
                if (response.success.boolValue) {
                    message = "send email code succcess";
                } else {
                    NSString *failStr = [NSString stringWithFormat:@"send email code fail：%@",response.msgKey];
                    message = [failStr UTF8String];
                }
                UnitySendMessage(gameObject, methodName, message);
            }];
        };
        MugenContainerVerifyView *verifyView = [[MugenContainerVerifyView alloc] init];
        [verifyView configVersion:MugenContainerReCaptureVersion.versionV3 language:@"hk" listener:(id <MugenContainerReCaptureResponseInterface>)delegate];
    } else {
        const char *gameObject = "MugenSDKMono";
        const char *methodName = "ReceiveMessageFromIOS";
        const char *message = "email is null";
        UnitySendMessage(gameObject, methodName, message);
    }
}

+ (void)getJwtToken
{
    NSString *jwtTokenKey = MugenContainerCommonConfigCompanion.companion.JWT_TOKEN;
    NSString *jwtToken = [[MugenContainerLocalStorage localStorage] getLocalValueByKeyKey:jwtTokenKey];
    const char *gameObject = "MugenSDKMono";
    const char *methodName = "ReceiveMessageFromIOS";
    const char *message = [jwtToken UTF8String];
    UnitySendMessage(gameObject, methodName, message);
}

+ (void)oAuthBind:(NSString *)provider email:(NSString *)email walletType:(NSString *)walletType
{
    //web3Auth login
    Web3AuthLoginDelegate *delegate = [[Web3AuthLoginDelegate alloc] init];
    delegate.web3AuthLoginCallBack = ^(id value) {
        MugenContainerAuthResponse *authResponse = value;
        MugenContainerLoginRepository *loginRepository = [[MugenContainerLoginRepository alloc] init];
        dispatch_main_async_safe(^{
            [loginRepository oauthBindIdToken:authResponse.idToken thirdAuthSource:MugenContainerThirdAuthType.externalSocial platform:@"web3auth" walletType:[self getWalletType:walletType] completionHandler:^(MugenContainerWebResult * _Nullable response, NSError * _Nullable error) {
                NSDictionary *resultDict = [MugenDataHelper convertToWebResult:response];
                NSString *jsonStr = [MugenDataHelper convertToString:resultDict];
                const char *gameObject = "MugenSDKMono";
                const char *methodName = "ReceiveMessageFromIOS";
                const char *message = [jsonStr UTF8String];
                UnitySendMessage(gameObject, methodName, message);
            }];
        });
    };
    [[MugenUnityManager sharedInstance].web3AuthClient signInSignProvider:[self getSingInProvider:provider] email:email walletType:[self getWalletType:walletType] web3AuthInterface:(id<MugenContainerWeb3AuthInterface>)delegate];
}

+ (void)emailBind:(NSString *)emailStr codeStr:(NSString *)codeStr type:(int)type
{
    if (emailStr.length > 0 && codeStr.length > 0) {
        MugenContainerBindType *bindType = MugenContainerBindType.bind;
        if ([MugenContainerBindType.bind getType] == type) {
            bindType = MugenContainerBindType.bind;
        } else if ([MugenContainerBindType.changeBind getType] == type) {
            bindType = MugenContainerBindType.changeBind;
        }
        MugenContainerLoginRepository *loginRepository = [[MugenContainerLoginRepository alloc] init];
        [loginRepository emailBindEmail:emailStr verifyCode:codeStr type:bindType tokenId:nil completionHandler:^(MugenContainerWebResultCustomerVO * _Nullable customer, NSError * _Nullable error) {
            NSDictionary *userDict = [MugenDataHelper convertToCustomerJsDict:customer];
            NSString *jsonStr = [MugenDataHelper convertToString:userDict];
            const char *gameObject = "MugenSDKMono";
            const char *methodName = "ReceiveMessageFromIOS";
            const char *message = [jsonStr UTF8String];
            UnitySendMessage(gameObject, methodName, message);
        }];
    } else {
        const char *gameObject = "MugenSDKMono";
        const char *methodName = "ReceiveMessageFromIOS";
        const char *message = "email or verifyCode is null";
        UnitySendMessage(gameObject, methodName, message);
    }
}

+ (void)logout
{
    MugenContainerLoginRepository *loginRepository = [[MugenContainerLoginRepository alloc] init];
    [loginRepository logoutWithCompletionHandler:^(MugenContainerWebResponse * _Nullable response, NSError * _Nullable error) {
        const char *gameObject = "MugenSDKMono";
        const char *methodName = "ReceiveMessageFromIOS";
        const char *message = "logout success";
        if (response.success.boolValue) {
            message = "logout success";
        } else {
            NSString *msg = [NSString stringWithFormat:@"logout fail: %@",response.msgKey];
            message = [msg UTF8String];
        }
        UnitySendMessage(gameObject, methodName, message);
    }];
}

+ (MugenContainerEnvironmentType *)getEnvironmentType:(NSString *)env
{
    MugenContainerEnvironmentType *envType = [MugenContainerStringKt_ toEnv:env];
    return envType;
}

+ (MugenContainerSignInProvider *)getSingInProvider:(NSString *)singInProvider
{
    MugenContainerSignInProvider *provider = [MugenContainerStringKt toSingInProvider:singInProvider];
    return provider;
}

+ (MugenContainerWalletType *)getWalletType:(NSString *)walletType
{
    MugenContainerWalletType *type = [MugenContainerStringKt toWalletType:walletType];
    return type;
}

+ (MugenContainerWeb3AuthNetWork *)getNetwork:(NSString *)web3Network
{
    MugenContainerWeb3AuthNetWork *network = [MugenContainerStringKt toWeb3netWork:web3Network];
    return network;
}

@end

@interface DeviceLoginDelegate()<MugenContainerLoginResponseInterface>

@end

@implementation DeviceLoginDelegate

- (void)loginResponseUserInfo:(MugenContainerUserInfo *)userInfo
{
    if (self.deviceLoginCallBack) {
        self.deviceLoginCallBack(userInfo);
    }
}

@end

@interface Web3AuthLoginDelegate()<MugenContainerWeb3AuthInterface>

@end

@implementation Web3AuthLoginDelegate

- (void)web3AuthResponseWeb3AuthResponse:(MugenContainerAuthResponse *)web3AuthResponse
{
    if (self.web3AuthLoginCallBack) {
        self.web3AuthLoginCallBack(web3AuthResponse);
    }
}

@end

@interface VerifyCodeDelegate()<MugenContainerReCaptureResponseInterface>

@end

@implementation VerifyCodeDelegate

- (void)responseResponse:(NSString *)response
{
    if (self.verifyCodeCallBack) {
        self.verifyCodeCallBack(response);
    }
}

@end
