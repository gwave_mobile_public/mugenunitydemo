//
//  MugenUnityManager.h
//  UnityFramework
//
//  Created by 杨鹏 on 2024/2/5.
//

#import <Foundation/Foundation.h>
@class MugenContainerWeb3AuthClient;
NS_ASSUME_NONNULL_BEGIN

@interface MugenUnityManager : NSObject

+ (instancetype)sharedInstance;

@property (nonatomic, strong, readonly) MugenContainerWeb3AuthClient *web3AuthClient;

@end

NS_ASSUME_NONNULL_END
