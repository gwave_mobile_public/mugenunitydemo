using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.Mugen.sdk
{


    public class MugenSDKMono : MonoBehaviour
    {

        public string clientId = "{Your App Token}";
        public string web3Network = "{Your App Secret}";
        public string url = "{Your App Secret}";

        private static MugenSDKMono _mugenSDK;
        public event EventHandler<string> OnLoginResponseReceived;


#if UNITY_IOS

#elif UNITY_ANDROID

#endif


        void Awake()
        {
            if (IsEditor()) 
            {
                return;
            }


            DontDestroyOnLoad(transform.gameObject);
            _mugenSDK = this;
        }

        void Start()
        {

#if UNITY_IOS
#elif UNITY_ANDROID
            MugenSDKAndroid.GetInstance().OnLoginResponseReceived += ReceiveMessageFromAndrod;
#endif
            MugenSDKMono.start(clientId, web3Network, url);

        }


        void OnDestroy()
        {
#if UNITY_IOS
#elif UNITY_ANDROID
        MugenSDKAndroid.GetInstance().OnLoginResponseReceived -= ReceiveMessageFromAndrod;
#endif
        }



        private void ReceiveMessageFromAndrod(object sender, string result)
        {
            if (OnLoginResponseReceived != null)
                OnLoginResponseReceived.Invoke(this, result);
        }


        void OnApplicationPause(bool pauseStatus)
        {
            
        }

        public void ReceiveMessageFromIOS(string result)
        {
            if (OnLoginResponseReceived != null)
                OnLoginResponseReceived.Invoke(this, result);

        }

        public static void CallDeviceLogin()
        {
#if UNITY_IOS
            MugenSDKiOS.CallDeviceLogin();
#elif UNITY_ANDROID
            MugenSDKAndroid.GetInstance().CallDeviceLogin();
#endif
        }

        public static void CallWeb3AuthSignIn(string provider,string email,Boolean autoRegister,Boolean web3Bind,string walletType)
        {
#if UNITY_IOS
            MugenSDKiOS.CallWeb3AuthSignIn(provider,email,autoRegister,web3Bind,walletType);
#elif UNITY_ANDROID
            MugenSDKAndroid.GetInstance().CallWeb3AuthSignIn(provider,email,autoRegister,web3Bind,walletType);
#endif
        }

        public static void CallGetUserInfo()
        {
#if UNITY_IOS
            MugenSDKiOS.CallGetUserInfo();
#elif UNITY_ANDROID
            MugenSDKAndroid.GetInstance().CallGetUserInfo();
#endif
        }

        public static void CallGetLocalUserInfo()
        {
#if UNITY_IOS
            MugenSDKiOS.CallGetLocalUserInfo();
#elif UNITY_ANDROID
            MugenSDKAndroid.GetInstance().CallGetLocalUserInfo();
#endif
        }

        public static void CallGetEmailCode(string email)
        {
#if UNITY_IOS
            MugenSDKiOS.CallGetEmailCode(email);
#elif UNITY_ANDROID
            MugenSDKAndroid.GetInstance().CallGetEmailCode(email);
#endif       
        }

        public static void CallQuickLogin(string email, string emailVerifyCode)
        {
#if UNITY_IOS
            MugenSDKiOS.CallQuickLogin(email, emailVerifyCode);
#elif UNITY_ANDROID
            MugenSDKAndroid.GetInstance().CallQuickLogin(email, emailVerifyCode);
#endif
        }

        public static void CallOAuthBind(string provider, string email,string walletType)
        {
#if UNITY_IOS   
            MugenSDKiOS.CallWeb3AuthSignIn(provider,email,true,true,walletType);
#elif UNITY_ANDROID
            MugenSDKAndroid.GetInstance().CallWeb3AuthSignIn(provider,email,true,true,walletType);
#endif            
        }

        public static void CallLogout()
        {
#if UNITY_IOS
            MugenSDKiOS.CallLogout();
#elif UNITY_ANDROID
            MugenSDKAndroid.GetInstance().CallLogout();
#endif            
        }

        public static void CallGetJwtToken()
        {
#if UNITY_IOS
            MugenSDKiOS.CallGetJwtToken();
#elif UNITY_ANDROID
            MugenSDKAndroid.GetInstance().CallGetJwtToken();
#endif            
        }

        public static void CallEmailBind(string email, string emailVerifyCode, int type)
        {
#if UNITY_IOS
            MugenSDKiOS.CallEmailBind(email,emailVerifyCode,8);
#elif UNITY_ANDROID
            MugenSDKAndroid.GetInstance().CallEmailBind(email,emailVerifyCode,8);
#endif
        }

        public static MugenSDKMono GetInstance()
        {
            return _mugenSDK;
        }

        public static void start(string clientId, string web3Network, string url)
        {
            if (IsEditor()) 
            {
                return;
            }

#if UNITY_IOS
            MugenSDKiOS.CallInitSDK("beta", "mugen", "BCP0RdVtKYtjMkKht8KfOvuRVoi-Xs4Di-iKwO04gA2AoNB5dt0eCtduhkF-wQpZXb4_jTgCAz6FwgHRcaL1jrw", "test", "https://kikitrade.com");
#elif UNITY_ANDROID
            MugenSDKAndroid.GetInstance().CallInitSDK("beta", "mugen", "BCP0RdVtKYtjMkKht8KfOvuRVoi-Xs4Di-iKwO04gA2AoNB5dt0eCtduhkF-wQpZXb4_jTgCAz6FwgHRcaL1jrw", "test", "com.unity3d.player://auth");
#elif (UNITY_WSA || UNITY_WP8)

#else

#endif
        }


        private static bool IsEditor()
        {
#if UNITY_EDITOR
            return true;
#else
            return false;
#endif
        }

    }
}
