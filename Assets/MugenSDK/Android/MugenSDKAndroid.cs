using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace com.Mugen.sdk
{

#if UNITY_ANDROID

    public class MugenSDKAndroid
    {
        private static AndroidJavaObject ajoCurrentActivity;
        private AndroidJavaObject kotlinObject;

        public event EventHandler<string> OnLoginResponseReceived;

        public static MugenSDKAndroid _mugenSDKAndroid = new MugenSDKAndroid();

        public static MugenSDKAndroid GetInstance()
        {
            return _mugenSDKAndroid;
        }

        public AndroidJavaObject GetAndroidInstance()
        {
            if (kotlinObject == null)
            {
                kotlinObject = new AndroidJavaClass("com.mugen.sdk.MugenUnityInstance");
            }

            if (ajoCurrentActivity == null)
            {
                ajoCurrentActivity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
            }

            if (ajoCurrentActivity == null)
            {
                Debug.Log("GetAndroidInstance ajoCurrentActivity is null");
            }

            if (kotlinObject == null)
            {
                Debug.Log("GetAndroidInstance fail");
            }
            return kotlinObject;
        }


        public void CallInitSDK(string environmentType, string network, string clientId, string web3Network, string url)
        {
            Debug.Log("CallInitSDK 1");
            GetAndroidInstance().CallStatic("initSDK", ajoCurrentActivity,environmentType, network, clientId, web3Network, url);
            Debug.Log("start to get local user info ");
            GetAndroidInstance().CallStatic("GetLocalUserInfo", new LoginResponseCallback(this));
        }

        public void CallInitWeb3Auth(string clientId, string web3Network, string url)
        {
            Debug.Log(string.Format("CallInit 1 {0}, {1} {2}", clientId, web3Network, url));
            GetAndroidInstance().CallStatic("initWeb3Auth", ajoCurrentActivity, clientId, web3Network, url);
            Debug.Log("CallInitWeb3Auth 2");
        }


        public void CallDeviceLogin()
        {
            GetAndroidInstance().CallStatic("DeviceLogin", new LoginResponseCallback(this));
        }

        public void CallWeb3AuthSignIn(string provider,string email,Boolean autoRegister,Boolean web3Bind,string walletType)
        
        {
            GetAndroidInstance().CallStatic("Web3AuthSign",provider,email,autoRegister,web3Bind,walletType,new LoginResponseCallback(this));
        }

        public void CallGetUserInfo()
        {
            GetAndroidInstance().CallStatic("GetUserInfo", new LoginResponseCallback(this));
        }

        public void CallGetLocalUserInfo()
        {
            GetAndroidInstance().CallStatic("GetLocalUserInfo", new LoginResponseCallback(this));
        }

        public void CallGetEmailCode(string email)
        {
            GetAndroidInstance().CallStatic("GetEmailCode",email, new LoginResponseCallback(this));
        }

        public void CallQuickLogin(string email, string emailVerifyCode)
        {
            GetAndroidInstance().CallStatic("EmailLogin",email,emailVerifyCode, new LoginResponseCallback(this));
        }

        public void CallOAuthBind(string idToken, string thirdAuthSource,string walletType)
        {
            GetAndroidInstance().CallStatic("OauthBind",idToken,thirdAuthSource,walletType, new LoginResponseCallback(this));
        }

        // 登出
        public void CallLogout()
        {
            GetAndroidInstance().CallStatic("Logout", new LoginResponseCallback(this));
        }

        public void CallGetJwtToken()
        {
            GetAndroidInstance().CallStatic("GetJwtToken", new LoginResponseCallback(this));
        }

        public void CallEmailBind(string email, string emailVerifyCode,int type)
        {
            GetAndroidInstance().CallStatic("EmailBind",email,emailVerifyCode,type,null, new LoginResponseCallback(this));
        }

        private class LoginResponseCallback : AndroidJavaProxy
        {
            private MugenSDKAndroid parent;


            public LoginResponseCallback(MugenSDKAndroid parent) : base("com.mugen.sdk.LoginResponseCallback") {
                this.parent = parent;
            }

            public void onResult(string result)
            {
                if (parent.OnLoginResponseReceived != null)
                {
                    parent.OnLoginResponseReceived.Invoke(this, result);
                }
            }
        }

    }

#endif
}