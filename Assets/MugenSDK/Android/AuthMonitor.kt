package com.mugen.sdk;

import com.kikitrade.login.AuthResponse
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

/**
 * @author: peter.qin@kikitrade.com
 * @date: 2024/1/25 at 17:15
 * @Description:
 */

class AuthMonitor {
    val _oauthEmitter = MutableStateFlow(AuthResponse())
    val authResponse: StateFlow<AuthResponse> = _oauthEmitter
}