package com.mugen.sdk

import android.app.Activity
import android.content.Context
import android.content.Intent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import com.kikitrade.login.*
import android.util.Log
import com.google.gson.Gson
import com.kikitrade.framework.CommonConfig
import com.kikitrade.framework.EnvironmentType
import com.kikitrade.framework.NetWork
import com.kikitrade.framework.coroutine.PageScope
import com.kikitrade.framework.coroutine.PageScopeManager
import com.kikitrade.framework.extensions.toEnv
import com.kikitrade.framework.extensions.toNetwork
import com.kikitrade.login.extentions.toBindType
import com.kikitrade.login.extentions.toSingInProvider
import com.kikitrade.login.extentions.toThirdAuthType
import com.kikitrade.login.extentions.toWalletType
import com.kikitrade.platform.ApplicationInitializer
import com.kikitrade.platform.outside.container.ApplicationContainer


interface LoginResponseCallback {
    fun onResult(result: String)
}


class MugenUnityInstance {

    companion object {
        private val loginRepository = LoginRepository()
        private lateinit var mContext: Context
        private lateinit var mClientId: String
        private lateinit var mWeb3Network: String
        private lateinit var mUrl: String
        lateinit var web3AuthClient: Web3AuthClient
        lateinit var container: ApplicationContainer
        var provider: SignInProvider = SignInProvider.GOOGLE
        var email: String? = null
        var clientId:String? = null
        var web3AUthUrl :String?=null
        var web3Network:String?=null
        /**
         * 初始化SDK
         * context: Application context
         */
        @JvmStatic
        fun initSDK(context: Context,env:String,network:String,clientId: String,web3Network: String,url:String) {
            mContext = context
            container = ApplicationContainer(context) {}
            ApplicationInitializer.initialize(
                env.toEnv(),
                container,
                network.toNetwork()
            )
            ApplicationInitializer.sassId = "mugen"
            this.clientId = clientId
            web3AUthUrl= url
            this.web3Network = web3Network
            Log.i("TAG", "===initSDK===,  ${env.toEnv()},${network.toNetwork()},$clientId,$web3Network,$url")

        }


        @JvmStatic
        fun initWeb3Auth(context: Context, clientId: String, web3Network: String, url: String) {
            mContext = context
            mClientId = clientId
            mWeb3Network = web3Network
            mUrl = url
            Log.i("TAG", "initWeb3Auth>>>>> ")
            CoroutineScope(Dispatchers.Main).launch {
                // Launch coroutine on the main dispatcher
                val activityClass = Activity::class.java
                val intent = Intent(context, activityClass)
                // ... (rest of your Intent handling code)

                lateinit var web3AuthNetWork: Web3AuthNetWork


                when (web3Network) {
                    "MAINNET" -> {
                        web3AuthNetWork = Web3AuthNetWork.MAINNET
                    }

                    "TESTNET" -> {
                        web3AuthNetWork = Web3AuthNetWork.TESTNET
                    }

                    "CYAN" -> {
                        web3AuthNetWork = Web3AuthNetWork.CYAN
                    }                // Default case
                    "SAPPHIRE_DEVNET" -> {
                        web3AuthNetWork = Web3AuthNetWork.SAPPHIRE_DEVNET
                    }

                    "SAPPHIRE_MAINNET" -> {
                        web3AuthNetWork = Web3AuthNetWork.SAPPHIRE_MAINNET
                    }

                    else -> {
                        web3AuthNetWork = Web3AuthNetWork.TESTNET
                    }
                }

                web3AuthClient = Web3AuthClient(mContext).initWeb3Auth(
                    clientId,
                    web3AuthNetWork,
                    url,
                    intent.data
                )
            }
        }

        /**
         *  web3Auth 登录
         *  autoRegister default is true, when false is change account
         */
        @JvmStatic
        fun Web3AuthSign(
            provider: String = "google",
            email: String?,
            autoRegister: Boolean = true,
            isWeb3Bind: Boolean = false,
            walletType: String,
            callback: LoginResponseCallback
        ) {
            Log.i("TAG", "Web3AuthSign====== ")
            this.provider = provider.toSingInProvider()
            this.email = email
            val intent = Intent(mContext, AuthLoginActivity::class.java)
            intent.apply {
                putExtra("clientId", clientId)
                putExtra("url", web3AUthUrl)
                putExtra("netWork", web3Network)
            }
            mContext.startActivity(intent)
            generatePageScope().launch {
                AuthLoginActivity.monitor.authResponse.collect {
                    if (it.idToken.isNullOrEmpty()) {
                        return@collect
                    }
                    when (!isWeb3Bind) {
                        true -> {
                            val result = loginRepository.oauthLogin(
                                idToken = it.idToken ?: "",
                                publicKey = it.publicKey ?: "",
                                autoRegister = autoRegister,
                                thirdAuthSource = ThirdAuthType.EXTERNAL_SOCIAL,
                                walletType = walletType.toWalletType()
                            )
                            callback.onResult(Gson().toJson(result))
                        }

                        else -> {
                            val result = loginRepository.oauthBind(
                                idToken = it.idToken ?: "",
                                thirdAuthSource = "EXTERNAL_SOCIAL".toThirdAuthType(),
                                walletType = walletType.toWalletType()
                            )
                            callback.onResult(Gson().toJson(result))
                        }
                    }
                }
            }

        }

        /**
         * 默认 通过设备登录
         */
        @JvmStatic
        fun DeviceLogin(callback: LoginResponseCallback) {
            CoroutineScope(Dispatchers.Main).launch {
                Log.d("DeviceLogin", "DeviceLogin 1")
                callback.onResult("回调测试")
                loginRepository.login(object : LoginResponseInterface {
                    override fun loginResponse(userInfo: UserInfo?) {
                        callback.onResult(Gson().toJson(userInfo))
                    }
                })
            }
        }

        /**
         * web3Auth 登录
         */
        @JvmStatic
        fun OauthLogin(
            idToken: String,
            publicKey: String,
            autoRegister: Boolean = true,
            thirdAuthSource: String = "EXTERNAL_SOCIAL",
            walletType: WalletType = WalletType.EVM,
            callback: LoginResponseCallback
        ) {
            val scope = PageScopeManager.getOrCreatePageScope(getUUID())
            scope.launch {
                val result = loginRepository.oauthLogin(
                    idToken = idToken,
                    publicKey = publicKey,
                    autoRegister = autoRegister,
                    thirdAuthSource = thirdAuthSource.toThirdAuthType(),
                    walletType = walletType
                )
                callback.onResult(Gson().toJson(result))
            }
        }


        @JvmStatic
        fun EmailLogin(
            email: String,
            emailVerifyCode: String,
            callback: LoginResponseCallback
        ) {
            val scope = PageScopeManager.getOrCreatePageScope(getUUID())
            scope.launch {
                val result = loginRepository.quickLogin(
                    email = email,
                    emailVerifyCode = emailVerifyCode,
                    tokenType = 2,
                    pTokenType = "android",
                    token = "null",
                    autoRegister = true
                )
                callback.onResult(Gson().toJson(result))
            }
        }

        @JvmStatic
        fun QuickLogin(
            email: String? = null,
            mobile: String? = null,
            password: String? = null,
            smsVerifyCode: String? = null,
            emailVerifyCode: String? = null,
            tokenType: Int? = null,
            source: Int? = null,
            token: String? = null,
            pTokenType: String? = null,
            deviceId: String? = null,
            userAgent: String? = null,
            smsType: Int? = null,
            inviteCode: String? = null,
            autoRegister: Boolean = true,
            callback: LoginResponseCallback
        ) {
            val scope = PageScopeManager.getOrCreatePageScope(getUUID())
            scope.launch {
                val result = loginRepository.quickLogin(
                    email = email,
                    mobile = mobile,
                    password = password,
                    smsVerifyCode = smsVerifyCode,
                    emailVerifyCode = emailVerifyCode,
                    tokenType = tokenType,
                    source = source,
                    token = token,
                    pTokenType = pTokenType,
                    deviceId = deviceId,
                    userAgent = userAgent,
                    smsType = smsType,
                    inviteCode = inviteCode,
                    autoRegister = autoRegister
                )
                callback.onResult(Gson().toJson(result))
            }
        }


        /**
         * 获取验证码
         */
        @JvmStatic
        fun GetEmailCode(email: String, callback: LoginResponseCallback) {
            val scope = PageScopeManager.getOrCreatePageScope(getUUID())
            scope.launch {
                val result = loginRepository.getEmailCode(email)
                val response = when (result.success) {
                    true -> {
                        "send email code success"
                    }

                    else -> {
                        "send email code error:${result.msgKey}"
                    }
                }
                callback.onResult(response)
            }
        }

        /**
         * 获取用户信息
         */
        @JvmStatic
        fun GetUserInfo(callback: LoginResponseCallback) {
            val scope = PageScopeManager.getOrCreatePageScope(getUUID())
            scope.launch {
                val result = loginRepository.getUserInfo()
                callback.onResult(Gson().toJson(result))
            }
        }

        /**
         *  登出
         */
        @JvmStatic
        fun Logout(callback: LoginResponseCallback) {
            generatePageScope().launch {
                val result = loginRepository.logout()
                val response = when (result.success) {
                    true -> {
                        "logout success"
                    }

                    else -> {
                        "logout error:${result.msgKey}"
                    }

                }
                callback.onResult(response)
            }

        }

        @JvmStatic
        fun GetJwtToken(callback: LoginResponseCallback) {
            callback.onResult(LocalStorage.getLocalValueByKey(CommonConfig.JWT_TOKEN))
        }


        /**
         * 绑定第三方账号
         */
        @JvmStatic
        fun OauthBind(
            idToken: String,
            thirdAuthSource: String = "EXTERNAL_SOCIAL",
            walletType: String = "evm",
            callback: LoginResponseCallback
        ) {
            val scope = PageScopeManager.getOrCreatePageScope(getUUID())
            scope.launch {
                val result = loginRepository.oauthBind(
                    idToken = idToken,
                    thirdAuthSource = thirdAuthSource.toThirdAuthType(),
                    walletType = walletType.toWalletType()
                )
                callback.onResult(Gson().toJson(result))
            }
        }

        /**
         * 绑定/换绑邮箱
         * BindType: BIND, CHANGE_BIND
         */
        @JvmStatic
        fun EmailBind(
            email: String,
            verifyCode: String,
            type: Int,
            tokenId: String? = null,
            callback: LoginResponseCallback
        ) {
            val scope = PageScopeManager.getOrCreatePageScope(getUUID())
            scope.launch {
                val result = loginRepository.emailBind(
                    email = email,
                    verifyCode = verifyCode,
                    type = type.toBindType(),
                    tokenId = tokenId
                )
                callback.onResult(Gson().toJson(result))
            }
        }

        /**
         * 当判断是否登录，调用 获取本地userInfo,解密jwt，exp 判断是否过期,过期则调用refreshToken
         */
        @JvmStatic
        fun GetLocalUserInfo(callback: LoginResponseCallback) {
            generatePageScope().launch {
                val result = loginRepository.getLocalUserInfo()
                callback.onResult(Gson().toJson(result))
            }
        }

        /**
         * 通过wallet 登录
         *
         */

        @JvmStatic
        fun LoginAuth(
            publicKey: String,
            walletType: WalletType = WalletType.EVM,
            nonce: String,
            signature: String,
            platform: String,
            callback: LoginResponseCallback
        ) {
            val scope = PageScopeManager.getOrCreatePageScope(getUUID())
            scope.launch {
                val result = loginRepository.loginAuth(
                    publicKey = publicKey,
                    walletType = walletType.type(),
                    nonce = nonce,
                    signature = signature,
                    platform = platform
                )
                callback.onResult(Gson().toJson(result))
            }
        }

        /**
         *  获取随机数
         */
        @JvmStatic
        fun GetAuthNonce(
            publicKey: String,
            callback: LoginResponseCallback
        ) {
            val scope = PageScopeManager.getOrCreatePageScope(getUUID())
            scope.launch {
                val result = loginRepository.getAuthNonce(
                    publicKey = publicKey
                )
                callback.onResult(Gson().toJson(result))
            }
        }

        /**
         * 绑定 wallet
         */
        @JvmStatic
        fun BindAuth(
            publicKey: String,
            address: String,
            nonce: String,
            signature: String,
            thirdAuthSource: ThirdAuthType = ThirdAuthType.EXTERNAL_WALLET,
            platform: String,
            walletType: WalletType = WalletType.EVM,
            saasId: String? = null,
            callback: LoginResponseCallback
        ) {
            val scope = PageScopeManager.getOrCreatePageScope(getUUID())
            scope.launch {
                val result = loginRepository.bindAuth(
                    publicKey = publicKey,
                    address = address,
                    nonce = nonce,
                    signature = signature,
                    thirdAuthSource = thirdAuthSource.source(),
                    platform = platform,
                    walletType = walletType.type(),
                    saasId = saasId
                )
                callback.onResult(Gson().toJson(result))
            }
        }


        private fun getUUID(): String {
            return java.util.UUID.randomUUID().toString()
        }

        private fun generatePageScope(): PageScope {
            return PageScopeManager.getOrCreatePageScope(getUUID())
        }
    }


}