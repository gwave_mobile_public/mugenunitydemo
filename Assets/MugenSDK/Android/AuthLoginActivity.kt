package com.mugen.sdk

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import com.google.gson.Gson
import com.kikitrade.login.AuthResponse
import com.kikitrade.login.WalletType
import com.kikitrade.login.Web3AuthClient
import com.kikitrade.login.Web3AuthInterface
import com.kikitrade.login.Web3AuthNetWork
import com.kikitrade.login.Web3WhiteLabelData
import com.kikitrade.login.extentions.toWeb3netWork
import com.unity3d.player.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AuthLoginActivity : Activity() {
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val clientId = intent.getStringExtra("clientId")?:""
        val url = intent.getStringExtra("url")?:""
        val netWork = intent.getStringExtra("netWork")?:""


        client = Web3AuthClient(this).initWeb3Auth(
            clientId, netWork.toWeb3netWork(),
            url,
            intent.data,
            Web3WhiteLabelData()
        )
        Log.i("TAG", "onCreate--> email-->${MugenUnityInstance.email},provider-->${MugenUnityInstance.provider} ")
        client?.signIn( MugenUnityInstance.provider,email = MugenUnityInstance.email, walletType = WalletType.EVM,object :
            Web3AuthInterface {
            override fun web3AuthResponse(web3AuthResponse: AuthResponse) {
                CoroutineScope(Dispatchers.Default).launch {
                    Log.i("TAG", "111,web3AuthResponse${Gson().toJson(web3AuthResponse)} ")
                    monitor._oauthEmitter.emit(web3AuthResponse)
                    this@AuthLoginActivity.finish()
                }
            }
        })
    }




    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        client?.handleSigning(intent?.data)
    }

    companion object{
        @SuppressLint("StaticFieldLeak")
        var client: Web3AuthClient? = null

        var monitor: AuthMonitor = AuthMonitor()
    }
}