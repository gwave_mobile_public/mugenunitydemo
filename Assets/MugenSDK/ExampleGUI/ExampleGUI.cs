﻿using System;
using System.Text;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;
using com.Mugen.sdk;

public class ExampleGUI : MonoBehaviour
{
    private int numberOfButtons = 13;
    private bool isEnabled;
    private bool showPopUp = false;
    private string txtSetEnabled = "Disable SDK";
    private string txtManualLaunch = "Manual Launch";
    private string txtSetOfflineMode = "Turn Offline Mode ON";
    private string txtLoginResponse = "test";

    private string emailInput = "input valid email address";

    private string newEmailInput="input new eamail";

    private string newEmailCode ="input new email code";

    private string emailCode = "";

    // 默认是登录，true 是绑定 
    private bool web3Bind = false;

    private string tokenId = "";

    void OnGUI()
    {
        GUIStyle buttonStyle = GUI.skin.button;
        buttonStyle.fontSize = 32;

        if (showPopUp)
        {
            GUI.Window(0, new Rect(150, 65, Screen.width - 300, Screen.height -130), ShowGUI, "LoginResonse");
        }

          

        if (GUI.Button(new Rect(150, Screen.height * 0 / numberOfButtons, Screen.width - 300, Screen.height / numberOfButtons), "CallDeviceLogin", buttonStyle))
        {
            MugenSDKMono.CallDeviceLogin();

            isEnabled = true;
            txtManualLaunch = "SDK Launched";
        }        
        
        if (GUI.Button(new Rect(150, Screen.height * 1 / numberOfButtons, Screen.width - 300, Screen.height / numberOfButtons), "Web3AuthLogin",buttonStyle))
        {
            MugenSDKMono.CallWeb3AuthSignIn("google",null,true,web3Bind,"evm");
            isEnabled = true;
        }
        
        if (GUI.Button(new Rect(150, Screen.height * 2 / numberOfButtons, Screen.width - 300, Screen.height / numberOfButtons), "GetLocalUserInfo", buttonStyle))
        {
            MugenSDKMono.CallGetLocalUserInfo();
            isEnabled = true;
        } 

        if (GUI.Button(new Rect(150, Screen.height * 3 / numberOfButtons, Screen.width - 300, Screen.height / numberOfButtons), "GetUserInfo"))
        {
            MugenSDKMono.CallGetUserInfo();
        }

        // 仅做 demo 演示用，实际使用时，不要在游戏中调用该接口
        if (GUI.Button(new Rect(150, Screen.height * 4 / numberOfButtons, Screen.width - 300, Screen.height / numberOfButtons), "OAuth Bind"))

        {
            MugenSDKMono.CallWeb3AuthSignIn("google",null,true,true,"evm");
        }

        if (GUI.Button(new Rect(150, Screen.height * 5 / numberOfButtons, (Screen.width - 300) / 2, Screen.height / numberOfButtons), "Logout"))
        {
            MugenSDKMono.CallLogout();
        }
        

        if (GUI.Button(new Rect(150 + (Screen.width - 300) / 2, Screen.height * 5 / numberOfButtons, (Screen.width - 300) / 2, Screen.height / numberOfButtons), "GetJwtToken "))
        {
            MugenSDKMono.CallGetJwtToken();
        }

        // email 登录 
        string labelText = " ==============>以下为: Email Login 功能区域<==============";
        GUI.Label(new Rect(150, Screen.height * 6 / numberOfButtons + 50, Screen.width - 300, Screen.height / numberOfButtons), labelText, new GUIStyle(GUI.skin.label) { fontSize = 20 });


        GUI.BeginGroup(new Rect(150, Screen.height * 7 / numberOfButtons, Screen.width - 300, Screen.height / numberOfButtons));
        GUIStyle emailFieldStyle = GUI.skin.textField;
        emailFieldStyle.fontSize = 20;

        float buttonWidth = (Screen.width - 300 - 50) / 2;
        float textFieldWidth = (Screen.width - 300 - 50) / 2;

        emailInput = GUI.TextField(new Rect(0, 0, textFieldWidth, Screen.height / numberOfButtons), emailInput, emailFieldStyle);
        if (GUI.Button(new Rect(textFieldWidth + 50, 0, buttonWidth, Screen.height / numberOfButtons), "Send Code"))
        {
            MugenSDKMono.CallGetEmailCode(emailInput);
            enabled = true;
        }

        GUI.EndGroup();

        GUI.BeginGroup(new Rect(150, Screen.height * 8 / numberOfButtons + 20, Screen.width - 300, Screen.height / numberOfButtons));
        GUIStyle codeFieldStyle = GUI.skin.textField;
        codeFieldStyle.fontSize = 20;

        float codeButtonWidth = (Screen.width - 300 - 50) / 2;
        float codeFieldWidth = (Screen.width - 300 - 50) / 2;

        emailCode = GUI.TextField(new Rect(0, 0, codeFieldWidth, Screen.height / numberOfButtons), emailCode, codeFieldStyle);
        if (GUI.Button(new Rect(codeFieldWidth + 50, 0, codeButtonWidth, Screen.height / numberOfButtons), "Email Login"))
        {
            MugenSDKMono.CallQuickLogin(emailInput, emailCode);
            enabled = true;
        }

        GUI.EndGroup();

        // change email 功能区域
        // 文本组件显示
         string bindlabelText = " ==============>以下为: Bind email 功能区域<==============";
        GUI.Label(new Rect(150, Screen.height * 9 / numberOfButtons+20 , Screen.width - 300, Screen.height / numberOfButtons), bindlabelText, new GUIStyle(GUI.skin.label) { fontSize = 20 });

        GUI.BeginGroup(new Rect(150, Screen.height * 10 / numberOfButtons-50, Screen.width - 300, Screen.height / numberOfButtons));
        GUIStyle newEmailFieldStyle = GUI.skin.textField;
        newEmailFieldStyle.fontSize = 20;

        float newButtonWidth = (Screen.width - 300 - 50) / 2;
        float newTextFieldWidth = (Screen.width - 300 - 50) / 2;
        newEmailInput = GUI.TextField(new Rect(0, 0, newTextFieldWidth, Screen.height / numberOfButtons), newEmailInput, newEmailFieldStyle);
        if (GUI.Button(new Rect(newTextFieldWidth + 50, 0, newButtonWidth, Screen.height / numberOfButtons), "Send New Email Code"))
        {
            MugenSDKMono.CallGetEmailCode(newEmailInput);
            enabled = true;
        }

        GUI.EndGroup();

        GUI.BeginGroup(new Rect(150, Screen.height * 11 / numberOfButtons + 20, Screen.width - 300, Screen.height / numberOfButtons));
        GUIStyle newCodeFieldStyle = GUI.skin.textField;
        newCodeFieldStyle.fontSize = 20;

        float newCodeButtonWidth = (Screen.width - 300 - 50) / 2;
        float newCodeFieldWidth = (Screen.width - 300 - 50) / 2;

        newEmailCode = GUI.TextField(new Rect(0, 0, newCodeFieldWidth, Screen.height / numberOfButtons), newEmailCode, newCodeFieldStyle);
        if (GUI.Button(new Rect(newCodeFieldWidth + 50, 0, newCodeButtonWidth, Screen.height / numberOfButtons), "Bind Email"))
        {
            MugenSDKMono.CallEmailBind(newEmailInput,newEmailCode,8);
            enabled = true;
        }

        GUI.EndGroup();
    }





    void Start()
    {
        MugenSDKMono.GetInstance().OnLoginResponseReceived += OnLoginResponseReceived;
    }


    void OnDestroy()
    {
        MugenSDKMono.GetInstance().OnLoginResponseReceived -= OnLoginResponseReceived;
    }

    private void OnLoginResponseReceived(object sender, string result)
    {
        txtLoginResponse = result;
        showPopUp = true;

        Debug.Log("OnLoginResponseReceived = " + result);

    }

    // 辅助方法，确保在主线程上调用
    public void Invoke(Action action)
    {
        if (action == null) throw new ArgumentNullException(nameof(action));
        StartCoroutine(InvokeCoroutine(action));
    }

    private IEnumerator InvokeCoroutine(Action action)
    {
        yield return null;
        action.Invoke();
    }

    void ShowGUI(int windowID)
    {
        GUIStyle labelStyle = GUI.skin.label;
        labelStyle.fontSize = 32;


        GUI.Label(new Rect(65, 40, Screen.width - 240, Screen.height - 210), txtLoginResponse);
       
        if (GUI.Button(new Rect(Screen.width / 2 - 240, Screen.height - 130 - 80, 120, 40), "OK"))
        {
            showPopUp = false;
        }
    }

    public void HandleGooglePlayId(String adId)
    {
        Debug.Log("Google Play Ad ID = " + adId);
    }

}
